@extends('layouts.app')

@section('content')


<!-- Add Action Button Here -->
    
   
<!--  -->
<div class="container">
    <div class="row"> 
        <div class="col-md-4">
          @include('layouts.sidebar')
        </div>
      <div class="card col-md-8">
        <span>Name: {{$invoice->firstname}} {{$invoice->lastname}}</span>
        <span>Phone Number: {{$invoice->phonenumber}}</span>
        <span>Address: {{$invoice->street_address}}, {{$invoice->baranggay}}</span>
        <span>Land Mark: {{$invoice->landmark ?? 'N/A'}}</span>
        <span>Order Date: {{$invoice->created_at}}</span>
        <span>Delivery Date: {{$invoice->delivery_date}}</span>
        <span>Special Request: {{$invoice->special_request}}</span>
      <table class="table">
        <tr>
          <td>Item</td>
          <td>Price</td>
          <td>Quantity</td>
          <td>Total</td>
        </tr>
        @foreach($orders as $order)
          <tr>
            <td>{{$order->product_name}}</td>
            <td>{{$order->price}}</td>
            <td>{{$order->quantity}}</td>
            <td>{{$order->total}}</td>
          </tr>
        @endforeach
      </table>
      <hr>
        <span> Total : {{$invoice->total}}</span>
      <hr>

      </div>
    </div>  
</div>
@endsection