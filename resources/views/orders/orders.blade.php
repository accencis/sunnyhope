
<div class="row">
             
               <table class="table">
                  <thead>
                    <tr>
                      <th scope="col"> Tracking #</th>	
                      <th scope="col">Customer Name</th>
                      <th scope="col">Phone Number</th>
                      <th scope="col">Total</th>
                      <th scope="col">Date Ordered</th>
                      <th scope="col">Delivery Date</th>
                    </tr>
                  </thead>
                  <tbody>
                   @foreach($invoices as $invoice) 
                    <tr>

             <td><a href="/seller/{{Auth::user()->token_id}}/{{$invoice->tracking_id}}">{{$invoice->tracking_id}}</a></td>
                      <td>{{$invoice->firstname}} {{$invoice->lastname}}</td>
                      <td>{{$invoice->phonenumber}}</td>	
                      <td>{{$invoice->total}}</td>
                      <td>{{$invoice->created_at}}</td>
                      <td>{{$invoice->delivery_date}}</td>
                     </tr>
                   @endforeach 
                  </tbody>
          </table>
              <!-- End of Orders -->
</div>

     