@extends('layouts.app')

@section('content')


<!-- Add Action Button Here -->
    
   



    
<input type="hidden" name="count" id="count" value="{{$count}}" >
<form method="post" action="/store/order">
@csrf

<div class="col-md-4">
  @include('layouts.sidebar')
</div>
<div class="col-md-8">
<div class="row" >

  <div class="col-md-12">
    
  <table class="table">

    <tr>
      <td><input class="form-control input-sm" type="text" name="firstname" placeholder="First Name" required></td>
      <td><input class="form-control input-sm" type="text" name="lastname" placeholder="Last Name"></td>
      <td><input class="form-control input-sm" type="text" name="phonenumber" placeholder="Phone Number" required></td>
      <td><input class="form-control input-sm" type="text" name="baranggay" placeholder="Baranggay" required></td>
    </tr>
    <tr>
      <td colspan="2"><input type="text" name="street_address" placeholder="Street Address" class="form-control input-sm" required></td>
      <td colspan="2"><input type="text" name="landmark" placeholder="Land Mark" class="form-control input-sm" required></td>

    </tr>
    <tr>
      
      <td><label>Delivery Date</label><input class="form-control input-sm" type="date" name="date" required></td>
      <td colspan="3"><label>Note</label><input class="form-control input-sm" type="text" name="note" placeholder="Special request"></td>
    </tr>
  </table>
  </div>  
</div>  

<div class="row">
  <p id="grandTotal">Total : 0</p>
</div>


<div class="row">
    
  <div class="card col-md-12">



  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Item</th>
        <th scope="col">Cost</th>
        <th scope="col">Quantity</th>
        <th scope="col">Total Price</th>
      </tr>
    </thead>
    <tbody>
      
      
      @foreach($products as $product)
        
        <tr>
          <input type="hidden" name="product_id[]" id="product_id{{$loop->iteration}}" value="{{$product->id}}" disabled>   
          <th scope="row"><input type="checkbox" onclick="checkbox()" name="items[]" id="items{{$loop->iteration}}" ></th>
          <td>{{$product->product_name}}</td>
          <td id="cost{{$loop->iteration}}">{{$product->price}}</td>
          
          <td><input id="quantity{{$loop->iteration}}" name="quantity[]" onchange="total()" type="number" step="0.25" min="0" width="3" value="0" disabled></td>
          <td class="total"><input type="" id="total{{$loop->iteration}}" name="total[]" size='4' disabled></td></td>
        </tr>
        
      @endforeach
    </tbody>
  </table>
      <button type="submit" class="btn btn-primary">
                                      {{ __('Add order') }}
      </button>
    </form>
  </div>
</div>


<script>
  var checkbox;
  var count = document.getElementById("count").value;
  
  function total(){
    var shit = 1;//kinahanglan pa taka ibutang para mo function lang???
    
    var quantity=[];
    var cost=[];
    var total = 0;
    var grandTotal = 0;
    var all;
    
    for(var i = 1; i<parseInt(count)+1;i++){
      quantity[i] = document.getElementById("quantity"+i).value;
      if(quantity[i].disabled){
        quantity[i] = 0;
      }
      else{
        quantity[i] = document.getElementById("quantity"+i).value;
      }
      
      
      cost[i] = document.getElementById("cost"+i).innerHTML;
      total = parseFloat(cost[i]) * parseFloat(quantity[i]);
      grandTotal = grandTotal +total;



      document.getElementById("total"+i).value = total+"";

      document.getElementById("grandTotal").innerHTML = "Total : <b>" + grandTotal +"</b>";


    }
  }


function checkbox(){
  for(var i = 1; i<parseInt(count)+1;i++){

    checkbox[i] = document.getElementById('items'+i);
      if(checkbox[i].checked == false){
        document.getElementById("quantity"+i).disabled = true;
        document.getElementById("total"+i).disabled = true;
        document.getElementById("product_id"+i).disabled = true;
        document.getElementById("quantity"+i).value = "0";
        document.getElementById("total"+i).innerHTML = "0";

      }
      else{
        document.getElementById("total"+i).disabled = false;
        document.getElementById("quantity"+i).disabled = false;
        document.getElementById("product_id"+i).disabled = false;
      }
  }   
}



</script>
<!-- Start Sidebar -->
       
    <!-- End Sidebar -->
   
@endsection
