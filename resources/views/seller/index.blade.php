@extends('layouts.app')

@section('content')


<!-- Add Action Button Here -->
    
   
<!--  -->
<div class="row">
    
    <div class="col-md-3">
      @include('layouts.sidebar')
    </div>
  <div class="card col-md-8">
    	 

      <ul class="nav nav-tabs pt-3">
        <li class="active"><a data-toggle="tab" href="#addOrder">Add</a></li>
        <li><a data-toggle="tab" href="#orders">Orders</a></li>
        <li><a data-toggle="tab" href="#menu1">To be delivered</a></li>
        <li><a data-toggle="tab" href="#menu2">Delivered</a></li>
        <li><a data-toggle="tab" href="#menu3">Total</a></li>
      </ul>

      <div class="tab-content">
        <div id="addOrder" class="tab-pane fade in active">
           @include('seller.orders')
        </div>

        <div id="orders" class="tab-pane fade ">
            
            @include('orders.orders')

        </div>
        <div id="menu1" class="tab-pane fade">
          <h3>To be delivered</h3>
          <p>To be Delivered</p>
        </div>
        <div id="menu2" class="tab-pane fade">
          <h3>Delivered</h3>
          <p>Delivered</p>
        </div>
        <div id="menu3" class="tab-pane fade">
          <h3>Total</h3>
          <p>Total</p>
        </div>
      </div>
  </div>

<!-- Start Sidebar -->
        
    <!-- End Sidebar -->

@endsection
