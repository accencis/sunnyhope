

<div class="col-md-12 mb-2">
           <div class="col-md-8">
	            <ul class="mr-auto list-group">
	            	<li class="list-group-item"><a href="">Home</a></li>
	            	<li class="list-group-item"><a href="/seller/{{Auth::user()->token_id}}">Orders</a></li>
                <li class="list-group-item"><a href="/">Products</a></li>
                <li class="list-group-item"><a href="/">Customers</a></li>
	            </ul>              
           </div>
        
      
      
        <div class="col-md-8 mb-2 mt-4"> 
			<div class="form-group ">       
           		<form method="post" action="/store/items" enctype="multipart/form-data">
           		@csrf
                @if (session('status'))
                  <div class="alert alert-success text-center" id="alert">
                  {{ session('status') }}
                  </div>
                @endif
           			<div class="form-group inline">
           				<label class="text-md-right">Product Name</label><br/>
           				<input type="text" name="product_name" class="col-md-12 @error('product_name') is-invalid @enderror"
           				value="{{ old('product_name') }}"
           				autocomplete="product_name" autofocus>
           				@error('product_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
						      @enderror
           			</div>
           			<div class="form-group inline">
           				<label class="text-md-right">Price</label><br/>
           				<input type="number" name="price" class="col-md-12 @error('price') is-invalid @enderror"
           				value="{{ old('price') }}"
           				autocomplete="price" autofocus>
           				@error('price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
						      @enderror
           			</div>
                <div class="form-group inline">
                  <label class="text-md-right">Step</label><br/>
                  <input type="number" name="step" class="col-md-12 @error('step') is-invalid @enderror"
                  value="{{ old('step') }}" step="0.25" min="0.25" 
                  autocomplete="step" autofocus>
                  @error('step')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                  @enderror
                </div>
                <div class="form-group inline">
                  <label class="text-md-right">Min Order</label><br/>
                  <input type="number" name="min" class="col-md-12 @error('min') is-invalid @enderror"
                  value="{{ old('min') }}"  min="0.25" step="0.25" 
                  autocomplete="step" autofocus>
                  @error('min')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                  @enderror
                </div>
           			<div class="form-group inline">
           				<label class="text-md-right">Upload Image</label><br/>
           				<input id="image_link" type="file" class="@error('image_link') is-invalid @enderror" name="image_link" value="{{ old('image_link') }}" required autocomplete="image_link" autofocus>
           			</div>
           			
           			<div class="">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add Product') }}
                                </button>
                            </div>
                        </div>
           		</form>
        	</div>
      	</div>

 	</div>

      <script>
$(function(){

   
    setTimeout(function(){
        $('#alert').fadeOut('slow');
    },5000);

});
</script>

