@extends('layouts.app')

@section('content')

<div class="container">
<input type="hidden" name="count" id="count" value="{{$count}}" >
<form method="post" action="/store/order">
@csrf	


<div class="row">
	@foreach($products as $product)
	<input type="hidden" name="product_id[]" id="product_id{{$loop->iteration}}" value="{{$product->id}}" disabled>
        <div class="col-md-2 mb-2">
            <div class="card text-center" >
               <img src="{{ asset('storage/'.$product->image_link) }}" class="w-100" >
                <label class="pt-1"><b>{{$product->product_name}}</b> - {{$product->price}}/kilo</label>
                <input type="checkbox" onclick="checkbox()" name="items[]" id="items{{$loop->iteration}}">
                <input id="quantity{{$loop->iteration}}" name="quantity[]" onchange="total()" type="number" step="0.25" min="0" width="3" value="0" disabled>
            </div>
        </div>
    @endforeach
       

</div>
<button type="submit" class="btn btn-primary">
                                    {{ __('Add order') }}
		</button>
	</form>

<script>
	var checkbox;
	var count = document.getElementById("count").value;
	
	function total(){
		var shit = 1;//kinahanglan pa taka ibutang para mo function lang???
		
		var quantity=[];
		var cost=[];
		var total = 0;
		var grandTotal = 0;
		var all;
		
		for(var i = 1; i<parseInt(count)+1;i++){
			quantity[i] = document.getElementById("quantity"+i).value;
			if(quantity[i].disabled){
				quantity[i] = 0;
			}
			else{
				quantity[i] = document.getElementById("quantity"+i).value;
			}
			
			
			cost[i] = document.getElementById("cost"+i).innerHTML;
			total = parseFloat(cost[i]) * parseFloat(quantity[i]);
			grandTotal = grandTotal +total;



			document.getElementById("total"+i).value = total+"";

			document.getElementById("grandTotal").innerHTML = "Total : <b>" + grandTotal +"</b>";


		}
	}


function checkbox(){
	for(var i = 1; i<parseInt(count)+1;i++){

		checkbox[i] = document.getElementById('items'+i);
			if(checkbox[i].checked == false){
				document.getElementById("quantity"+i).disabled = true;
				document.getElementById("total"+i).disabled = true;
				document.getElementById("product_id"+i).disabled = true;
				document.getElementById("quantity"+i).value = "0";
				document.getElementById("total"+i).innerHTML = "0";

			}
			else{
				document.getElementById("total"+i).disabled = false;
				document.getElementById("quantity"+i).disabled = false;
				document.getElementById("product_id"+i).disabled = false;
			}
	}		
}



</script>

@endsection
