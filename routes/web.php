<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index');


Route::group(['middleware' => 'App\Http\Middleware\CheckRole'], function()
{
Route::get('/home', 'HomeController@index')->middleware('auth');
Route::get('/order', 'OrderController@index')->middleware('auth');

Route::get('/seller/{tokenid}', 'ProductController@index')->middleware('auth');
Route::get('/seller/{tokenid}/{tracking_id}', 'ProductController@customer')->middleware('auth');
});
Route::get('/admin', 'AdminController@index')->middleware('auth');
Route::post('/store/order', 'OrderController@store');



Route::get('/add/product', 'ProductController@create')->middleware('auth');


	Route::post('/store/items', 'ProductController@store')->middleware('auth');
//Route::post('/store/items',array('as'=>'store','uses'=>'ProductController@store'));
Route::get('/refresh', function () {
  Artisan::call('view:clear');
  return back();
});