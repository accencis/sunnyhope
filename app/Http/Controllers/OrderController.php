<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Customers;
use App\Invoice;
use DB;
use Str;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = DB::table('products')->get();
        $count = DB::table('products')->count();
        return view('seller.orders',compact('products','count'));
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd(auth()->user()->id);
      $grand_total = 0;
      $data = $request->all();
      $user_id = 0;
      $invoice = new Invoice;
      
      if(isset(auth()->user()->id)){
        $user_id = auth()->user()->id;
      }
      else{
        $user_id = 0;
      }

      $customer = new Customers;

      $cust = DB::table('customers')->where('phonenumber',$request->phonenumber)->first();

      //dd($cust->id);
      $tracking_id = Str::random(10);  
      if($cust !== null){

        $cust_id = $cust->id;
        $invoice->customer_id = $cust_id;
      }
      else{

          $customer->firstname = $request->firstname;
          $customer->lastname = $request->lastname;
          $customer->phonenumber = $request->phonenumber;
          $customer->baranggay = $request->baranggay;
          $customer->street_address = $request->street_address;
          $customer->landmark = $request->landmark;
          $customer->user_id = $user_id;
          //dd($customer);
          $customer->save();  
          $cust_id = $customer->id;
          
          $invoice->customer_id = $cust_id;
    }


     
         
      foreach ($request->product_id as $key => $value) {
           
          $data = array(
                         'user_id'=>$user_id,
                         'cust_id'=>$cust_id,
                         'item_id'=>$request->product_id[$key], 
                         'quantity'=>$request->quantity[$key],
                         'total'=>$request->total[$key],
                         'tracking_id'=>$tracking_id                      
            );
         $grand_total +=  $request->total[$key];
         Order::create($data);           
}

    //dd($grand_total);
     

     $invoice->tracking_id = $tracking_id;
     $invoice->delivery_date = $request->date;
     $invoice->total = $grand_total;
     $invoice->special_request = $request->note;
     
     // $invoice->is_delivered = 0;
     $invoice->save();
      
    
      //dd($finalArray);
      // foreach($prod_ids as $key => $value) {
      //    $data = request()->validate([
      //           'product_id' =>'', 
      //           'quantity'=>'',
      //           'items'=>'',
      //           'total'=>'',
      //           ]); 

      //    dd($data);
      //    //dd($data);
       //    auth()->user()->order()->create([
       //      'product_id' => $data['product_id'],
       //      'quantity' => $data['quantity'],
       //      'total'=>$data['total'],


       // ]); 


         // $orders = new Order();
         // $orders->user_id = auth()->user()->id;
         // //$orders->product_id = $data['product_id'];
         // $orders->quantity = $data['quantity'];
         // // $orders->total = $data['total'];

         // $orders->quantity = explode(",", $orders->quantity);
         // $orders->save();
         // //dd($orders);
         //problem cannot store multiple data to the database


      
    // }  
return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        //return view('seller.index',compact('invoices'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
