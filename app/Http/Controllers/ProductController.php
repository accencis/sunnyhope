<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')->get();
        $count = DB::table('products')->count();
        
        $products = DB::table('products')
        ->where('user_id',auth()->user()->id)
        ->get();

// ->leftJoin('customers','orders.cust_id','=','customers.id')
//                 
//                 
        $invoices = DB::table('invoices')->where('is_delivered',null)
                ->rightJoin('customers','invoices.customer_id','=','customers.id')
                ->orderBy('invoices.created_at','desc')
                ->get();

          //dd($invoices);
        


        return view('seller.index',compact('products','count','invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function customer($token_id, $tracking_id){

            $invoice = DB::table('invoices')
                ->where('tracking_id',$tracking_id)
                ->where('is_delivered',null)
                ->rightJoin('customers','invoices.customer_id','=','customers.id')
                ->first();

            $orders = DB::table('orders')->where('tracking_id',$invoice->tracking_id)
                ->rightJoin('products','orders.item_id','=','products.id')
            ->get();    
          // dd($orders);

            return view('orders.customer',compact('invoice','orders'));

    }
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $data = request()->validate([
                'product_name' => 'required', 
                'price'=>'required|regex:/^\d+(\.\d{1,2})?$/', 
                'image_link'=>['image','mimes:jpeg,bmp,png,jpg','max:10240'],
                'step'=>'',
                'min'=>'',
             
        ]);
        $image_path = request('image_link')->store('uploads','public');

        //dd(request('image_link')->store('uploads','public'));

        auth()->user()->product()->create([
            'product_name' =>$data['product_name'],
            'price'=>$data['price'],
            'step'=>$data['step'],
            'min'=>$data['min'],
            'image_link'=>$image_path,
        ]);

        return back()->with('status', 'Product Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
